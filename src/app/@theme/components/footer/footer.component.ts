import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Created with ♥ by <b><a href="https://Spacer.com" target="_blank">Spacer</a></b> 2018</span>
    <div class="socials">
      <a href="https://www.facebook.com/SpacerAustralia" target="_blank" class="ion ion-social-facebook"></a>
      <a href="https://twitter.com/SpacerAu " target="_blank" class="ion ion-social-twitter"></a>
      <a href="https://www.linkedin.com/company/spacer" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}
