[![Build Status](https://travis-ci.org/Spacer/ngx-admin.svg?branch=master)](https://travis-ci.org/Spacer/ngx-admin)
[![Join the chat at https://gitter.im/ng2-admin/Lobby](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/ng2-admin/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Dependency Status](https://david-dm.org/Spacer/ngx-admin/status.svg)](https://david-dm.org/Spacer/ng2-admin)
# Admin template based on Angular 4+, Bootstrap 4 and <a href="https://github.com/Spacer/nebular">Nebular</a>
<a target="_blank" href="http://Spacer.com/ngx-admin/"><img src="https://i.imgur.com/XoJtfvK.gif"/></a>

### What's included:

- Angular 4+ & Typescript
- Bootstrap 4+ & SCSS
- Responsive layout
- High resolution
- Flexibly configurable themes with **hot-reload** (2 themes included)
- Authentication module with multiple providers
- Lots of awesome features:
  - Buttons
  - Modals
  - Icons
  - Typography
  - Animated searches
  - Forms
  - Tabs
  - Notifications
  - Tables
  - Maps
  - Charts
  - Editors
  

### From Spacer
Made with :heart: by [Spacer team](http://Spacer.com/). Follow us on [Twitter](https://twitter.com/Spacer_inc) to get the latest news first!
We're always happy to receive your feedback!
